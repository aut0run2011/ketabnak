<?php


        // Include the template engine and Database Class
        include('Classes/class.template.inc');
        include('Classes/class.db.php');


        // Creare a database instance
        $Dbase = new Dbase();

        // Created proper variables
        $page_title = 'کتابناک';
        $page_heading = 'افزودن کتاب';
        $page_subtitle = 'هر کتاب، فرصت یک زندگی تازه';


        // Create the Books table if it's not created
        $create_table_query = 'CREATE TABLE IF NOT EXISTS  Books  
                                (id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
                                title VARCHAR(90) NOT NULL,
                                reg_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP)';
        $Dbase->execute($create_table_query);



        // Get the book list from the database
        $sql = "SELECT * FROM `dl1_downloads` ORDER BY dlid DESC LIMIT 50";
        $books = $Dbase->getAll($sql);


        // Handle the template engine
        $tpl = new template;

        // Load the filed
        $tpl->load_file('header', 'templates/header-template.html');
        $tpl->load_file('main', 'templates/main-template.html');

        // Register the variables
        $tpl->register('main', 'page_heading, page_title, page_subtitle');

        // Parse needed loops for showing books
        $tpl->parse('header, main');
        $tpl->parse_loop('main', 'books');

        // Render the result!
        $tpl->print_file('header, main');

?>