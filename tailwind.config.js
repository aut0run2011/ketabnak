module.exports = {
  theme: {

    transitionProperty: { // defaults to these values
      'none': 'none',
      'all': 'all',
      'color': 'color',
      'bg': 'background-color',
      'border': 'border-color',
      'colors': ['color', 'background-color', 'border-color'],
      'opacity': 'opacity',
      'transform': 'transform',
    },
    transitionDuration: { // defaults to these values
      'default': '250ms',
      '0': '0ms',
      '100': '100ms',
      '250': '250ms',
      '500': '500ms',
      '750': '750ms',
      '1000': '1000ms',
    },
    transitionTimingFunction: { // defaults to these values
      'default': 'ease',
      'linear': 'linear',
      'ease': 'ease',
      'ease-in': 'ease-in',
      'ease-out': 'ease-out',
      'ease-in-out': 'ease-in-out',
    },
    transitionDelay: { // defaults to these values
      'default': '0ms',
      '0': '0ms',
      '100': '100ms',
      '250': '250ms',
      '500': '500ms',
      '750': '750ms',
      '1000': '1000ms',
    },
    willChange: { // defaults to these values
      'auto': 'auto',
      'scroll': 'scroll-position',
      'contents': 'contents',
      'opacity': 'opacity',
      'transform': 'transform',
    },
    extend: {
      colors: {
        'blue-main' : '#0027fd',
        gray: {
          '100': '#FBFBFB',
          '200': '#CDCDCD',
          '300': '#AFAFAF',
          '400': '#727272',
          '500': '#363636',
          '600': '#313131',
          '700': '#202020',
          '800': '#181818',
          '900': '#101010',
          },
          silver: {
            '100': '#F7FAFC',
            '200': '#EDF2F7',
            '300': '#E2E8F0',
            '400': '#CBD5E0',
            '500': '#A0AEC0',
            '600': '#718096',
            '700': '#4A5568',
            '800': '#2D3748',
            '900': '#1A202C',
          }
      }
    }
  },
  variants: { // all the following default to ['responsive']
  backgroundColor: ['responsive', 'hover', 'focus', 'active', 'group-hover'],
  transitionProperty: ['responsive'],
  transitionDuration: ['responsive'],
  transitionTimingFunction: ['responsive'],
  transitionDelay: ['responsive'],
  willChange: ['responsive'],
},
  plugins: [
    require('tailwindcss-transitions')(),
  ]
}
