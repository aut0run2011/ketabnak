<?php


        // Include the template engine and Database Class
        include('Classes/class.template.inc');
        include('Classes/class.db.php');

	require 'start.php';

        // Creare a database instance
        $Dbase = new Dbase();

        // Created proper variables
        $page_title = 'کتابناک';
        $page_heading = 'جستجوی کتاب';
        $page_subtitle = 'هر کتاب، فرصت یک زندگی تازه';
        $search_result = []; 


        // Get the book list from the database
        $sql = "SELECT * FROM `books`";
        $search_result = [];


        // Handle the template engine
        $tpl = new template;

        // Load the filed
        $tpl->load_file('header', 'templates/header-template.html');
        $tpl->load_file('main', 'templates/search-template.html');

        // Register the variables
        $tpl->register('main', 'page_heading, page_title, page_subtitle');

        // Parse needed loops for showing books
        $tpl->parse('header, main');
        $tpl->parse_loop('main', 'books');
        $tpl->parse_loop('main', 'search_result');

        // Render the result!
        $tpl->print_file('header, main');

?>