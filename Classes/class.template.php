<?php

/**
 * class engineTemplate
 *
 * Class-e Template
 * This class handles all of template commands and controls
 * Based on the template class by Richard Heyes (version 1.4)
 *
 * @access public
 */
class engineTemplate {

    /**
     * template::$var_names
     *
     * Description of the variables within the templates
     * @var array
     */
    var $var_names	= array();

    /**
     * template::$var_global
     *
     * Description of the variables within the templates that are used for all templates
     * @var array
     */
    var $var_global	= array();

    /**
     * template::$files
     *
     * Keeps Array () with the individual's template file_id
     * @var array
     */
    var $files		= array();

    /**
     * template::$folder
     *
     * Path to the folder where the files are
     * @var string
     */
    var $folder		= '';

    /**
     * template::$start
     *
     * Initially identifiers for variables
     * @var string
     */
    var $start		= '{';

    /**
     * template::$end
     *
     * End Identifier Variables
     * @var string
     */
    var $end		= '}';

    /**
     * template::$header_file
     *
     * Variable's own header file
     * @var string
     */
    var $header_file = '';

    /**
     * template::$footer_file
     *
     * Variable's own footer file
     * @var string
     */
    var $footer_file = '';

    /**
     * template::$key_cache
     *
     * Variables in templates
     * @var array
     */
    var $key_cache = array();

    /**
     * template::$val_cache
     *
     * Value for variables in the template
     * @var array
     */
    var $val_cache = array();

    /**
     * template::engineTemplate()
     *
     * Constructor
     * @access public
     */
    function engineTemplate($folder='') {
        $this->folder = $folder;
    }

    /**
     * template::setOwnBorder()
     *
     * Sets the path for its own header and footer files
     * @access public

    function setOwnBorder($footer = NULL, $header = NULL) {
        if($header && is_file($header)) {
        $this->header_file = $header;
        } else {
        die("Path to the specified header file is not correct. Could not find file");
        }
        if($footer && is_file($footer)) {
        $this->footer_file = $footer;
        } else {
        die("Path to the specified header file is not correct. Could not find file");
        }
    }
     */

    /**
     * template::load_file()
     *
     * Loads a template file in the class
     * @access public
     */
    function loadFile($file_id, $filename) {

        if (empty($this->folder))
            $this->folder = template_path;

        $loadfile = $this->folder.$filename;

        $this->files[$file_id] = fread($fp = fopen($loadfile, 'r'), filesize($loadfile));
        if(!fclose($fp)) {
            return false;
        }
    }

    /**
     * template::setIdentifiers()
     *
     * Set the start and end identifiers for variable
     * @access public
     */
    function setIdentifiers($start, $end) {
        $this->start = $start;
        $this->end = $end;
    }

    /**
     * template::traverseArray()
     *
     * This is used when an array is Registered as a variable.
     * @access private
     */
    function traverseArray($file_id, $array) {
        while(list(,$value) = each($array)){
            if(is_array($value)) {
                $this->traverse_array($file_id, $value);
            } else {
                $this->var_names[$file_id][] = $value;
            }
        }
    }

    /**
     * template::register()
     *
     * Initialization of a tag $ var_name deposit and the corresponding value of $ value. Also passing an array possible with appropriate mating
     *
     * @param $var_name variable name
     * @param $value variable value
     * @access public
     * @return
     */
    function register($var_name, $value="") {
        if (!is_array($var_name)) {
            if (!empty($var_name)) {
                $value = preg_replace(array('/\$([0-9])/', '/\\\\([0-9])/'), array('&#36;\1', '&#92;\1'), $value);
                $this->key_cache[$var_name] = "/".$this->addDelemiter($var_name)."/";
                $this->val_cache[$var_name] = $value;
            }
        } else {
            foreach ($var_name as $key => $val) {
                if (!empty($key)) {
                    $val = preg_replace(array('/\$([0-9])/', '/\\\\([0-9])/'), array('&#36;\1', '&#92;\1'), $val);
                    $this->key_cache[$key] = "/".$this->addDelemiter($key)."/";
                    $this->val_cache[$key] = $val;
                }
            }
        }
        return;
    }

    /**
     * template::add_delemiter()
     * Adds Variablenlimitierung added
     *
     * @param string $var_name Variable name
     * @access private
     * @return
     */
    function addDelemiter($var_name) {
        return preg_quote($this->start.$var_name.$this->end);
    }

    /**
     * template::registerGlobal()
     *
     * Variables registration
     * @access public
     */
    function registerGlobal($var_name) {
        $this->var_global[] = $var_name;
    }

    /**
     * template::includeFile()
     *
     * Adds external file, such as header / footer
     * @access private
     */
    function includeFile($file_id, $filename) {
        if(file_exists($filename)) {
            $include = fread($fp = fopen($filename, 'r'), filesize($filename));
            fclose($fp);
        } else {
            $include = '[ERROR: "'.$filename.'" does not exist.]';
        }
        $tag = substr($this->files[$file_id], strpos(strtolower($this->files[$file_id]), '<include filename="'.$filename.'">'), strlen('<include filename="'.$filename.'">'));
        $this->files[$file_id] = str_replace($tag, $include, $this->files[$file_id]);
    }

    /**
     * template::parse()
     *
     * Template parse and possibly replace INCLUDE TAGS by files; substituting variables
     * @access public
     */
    function parse($file_id) {
        $file_ids = explode(',', $file_id);
        for(reset($file_ids); $file_id = trim(current($file_ids)); next($file_ids)){
            while(is_long($pos = strpos(strtolower($this->files[$file_id]), '<include filename="'))){
                $pos += 19;
                $endpos = strpos($this->files[$file_id], '">', $pos);
                $filename = substr($this->files[$file_id], $pos, $endpos-$pos);
                $this->includeFile($file_id, $filename);
            }

            $this->parseAllVars($file_id);

            if(isset($this->var_global) AND count($this->var_global) > 0){
                for($i=0; $i<count($this->var_global); $i++){
                    $temp_var = $this->var_global[$i];
                    global $$temp_var;
                    $this->files[$file_id] = str_replace($this->start.$temp_var.$this->end, $$temp_var, $this->files[$file_id]);
                }
            }
        }
    }

    /**
     * template::parseAllVars()
     *
     * Placeholders in the templates to replace
     * @access public
     */
    function parseAllVars($file_id) {
        ini_set("memory_limit","53M");

        $this->files[$file_id] = preg_replace($this->key_cache, $this->val_cache, $this->files[$file_id]);
    }

    /**
     * template::parseLoop()
     *
     * Loop based on an array's. Array key and wildcard must match
     * @access public
     */
    function parseLoop($file_id, $array_name) {
        global $$array_name;
        $loop_code = '';

        $start_pos = strpos(strtolower($this->files[$file_id]), '<loop name="'.$array_name.'">') + strlen('<loop name="'.$array_name.'">');
        $end_pos = strpos(strtolower($this->files[$file_id]), '</loop name="'.$array_name.'">');

        $loop_code = substr($this->files[$file_id], $start_pos, $end_pos-$start_pos);

        $start_tag = substr($this->files[$file_id], strpos(strtolower($this->files[$file_id]), '<loop name="'.$array_name.'">'),strlen('<loop name="'.$array_name.'">'));
        $end_tag = substr($this->files[$file_id], strpos(strtolower($this->files[$file_id]), '</loop name="'.$array_name.'">'),strlen('</loop name="'.$array_name.'">'));

        if($loop_code != '') {
            $new_code = '';
            for($i=0; $i<count($$array_name); $i++) {
                $temp_code = $loop_code;
                foreach (${$array_name}[$i] as $key => $value) {
                    // while(list($key,) = each(${$array_name}[$i])) {
                    $temp_code = str_replace($this->start.$key.$this->end,${$array_name}[$i][$key], $temp_code);
                }
                $new_code .= $temp_code;
            }

            $this->files[$file_id] = str_replace($start_tag.$loop_code.$end_tag, $new_code, $this->files[$file_id]);
        }

    }

    /**
     * template::parseIf()
     *
     * Evaluation of the IF condition and corresponding parse condition
     * @access public
     */
    function parseIf($file_id, $array_name){
        $var_names = explode(',', $array_name);

        for($i=0; $i<count($var_names); $i++) {
            $if_code	= '';
            $start_pos	= strpos(strtolower($this->files[$file_id]), '<if name="'.strtolower($var_names[$i]).'">') + strlen('<if name="'.strtolower($var_names[$i]).'">');
            $end_pos	= strpos(strtolower($this->files[$file_id]), '</if name="'.strtolower($var_names[$i]).'">');

            $if_code	= substr($this->files[$file_id], $start_pos, $end_pos-$start_pos);
            $start_tag	= substr($this->files[$file_id], strpos(strtolower($this->files[$file_id]), '<if name="'.strtolower($var_names[$i]).'">'),strlen('<if name="'.strtolower($var_names[$i]).'">'));
            $end_tag	= substr($this->files[$file_id], strpos(strtolower($this->files[$file_id]), '</if name="'.strtolower($var_names[$i]).'">'),strlen('</if name="'.strtolower($var_names[$i]).'">'));

            $new_code = '';
            if($if_code != '') {
                global ${$var_names[$i]};
                if(@${$var_names[$i]}) $new_code = $if_code;
                $this->files[$file_id] = str_replace($start_tag.$if_code.$end_tag, $new_code, $this->files[$file_id]);
            }
        }
    }

    /**
     * template::removeTags()
     * Remove empty placeholder IF statements to remove
     *
     * @param $template Templatename
     * @access private
     * @return
     */
    function removeTags($file_id) {
        $search = array("/".$this->start."+[^ \t\r\n]+".$this->end."/","/&#36;([0-9])/","/&#92;([0-9])/","`<\!#.*?#\!>`s");
        $replace = array("","$\1","\\\1","");
        $file_id = preg_replace($search, $replace, $file_id);
        return $file_id;
    }

    /**
     * template::printFile()
     *
     * Prints parsted template in the browser
     * @access public
     */
    function printFile($file_id, $ispopup='') {
        if(is_long(strpos($file_id, ',')) == TRUE){
            $file_id = explode(',', $file_id);
            for(reset($file_id);
                $current = current($file_id);
                next($file_id)) $template = $this->removeTags($this->files[trim($current)]);
        } else {
            $template = $this->removeTags($this->files[$file_id]);
        }

        $this->printGzipFile($template);
    }

    /**
     * template::printGzipFile()
     *
     * Forwards parsted template in the GZIP output, if enabled and available
     * @access private
     */
    function printGzipFile($template) {
        if(!$template) {
            trigger_error("هیچ خروجی برای فشرده سازی و ارسال به کاربر تولید نشده است",E_USER_WARNING);
        }

        if(!defined("IS_POPUP")) define('IS_POPUP',false);

        // header('Content-type: text/html; charset=utf-8');
        if ($this->header_file != "" && !IS_POPUP) include($this->header_file);
        echo $template;
        if ($this->footer_file != "" && !IS_POPUP) include($this->footer_file);
        // flush();
    }

    /**
     * template::returnFile()
     *
     * Give parsted back without this template to send to the browser.
     * @access public
     */
    function returnFile($file_id){
        $ret = '';
        if(is_long(strpos($file_id, ',')) == TRUE) {
            $file_id = explode(',', $file_id);
            for(reset($file_id); $current = current($file_id); next($file_id)) $ret .= $this->files[trim($current)];
        } else {
            $ret .= $this->files[$file_id];
        }
        return $this->removeTags($ret);
    }

    /**
     * template::pprint()
     *
     * Registered variable and parses the specified template, template is followed by output in the browser and can be further
     * @access public
     * @param string $replacements
     */
    function pprint($file_id, $replacements = ''){
        $this->register($file_id, $replacements);
        $this->parse($file_id);
        $this->printFile($file_id);
    }

    /**
     * template::pget()
     *
     * Registered variable and parses the specified template, template is not followed to the browser and can be further
     * @access public
     * @param string $replacements
     * @return engineTemplate
     */
    function pget($file_id, $replacements = ''){
        $this->register($file_id, $replacements);
        $this->parse($file_id);
        return $this->returnFile($file_id);
    }

    /**
     * template::pprintFile()
     *
     * Loading template parsing variables in the template and returns the parsed template (will be displayed in the browser)
     * @access public
     * @param string $replacements
     */
    function pprintFile($filename, $replacements = ''){
        for($file_id=1; isset($this->files[$file_id]); $file_id++);
        $this->loadFile($file_id, $filename);
        $this->pprint($file_id, $replacements);
        //unset($this->files[$file_id]); //We do not unset anymore, because we are at the end of page
    }

    /**
     * template::pgetFile()
     *
     * Loading template parsing variables in the template and returns the parsed template (will not be displayed in the browser)
     * @access public
     * @param string $replacements
     * @return engineTemplate
     */
    function pgetFile($filename, $replacements = ''){
        for($file_id=1; isset($this->files[$file_id]); $file_id++);
        $this->loadFile($file_id, $filename);
        return $this->pget($file_id, $replacements);
    }
}