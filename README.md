# Ketabnak


## Prerequisites:

 1. a working web server like apache or lightspeed
 2. latest version of npm
 3. PHP +5
 4. MySQL +5.5

## How to install:
 1. Clone the repository
 2. modify your database credentials in the `Classes/class.db.php`
 3. `npm install`
 4.  `npx tailwind build style.css -o css/main.css`