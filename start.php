<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);

$fuzzy_prefix_length  = 2;
$fuzzy_max_expansions = 50;
$fuzzy_distance       = 3; //represents the levenshtein distance;

use TeamTNT\TNTSearch\TNTSearch;

require 'database.php';

require 'vendor/autoload.php';


$tnt = new TNTSearch;

$tnt->loadConfig([
    'driver'    => 'mysql',
    'host'      => 'localhost',
    'database'  => 'ketabnak_db',
    'username'  => 'root',
    'password'  => 'toor',
    'storage'   => '/var/www/html/tntsearch/examples/',
	'charset' => 'utf8',
	'collation' => 'utf8_general_ci'
]);

$tnt->fuzziness = true;

// Books indexer
// $indexer = $tnt->createIndex('ketabnak.books');
// $indexer->setPrimaryKey('dlid');
// $indexer->query('SELECT dlid, dltitle FROM dl1_downloads;');
// $indexer->run();


// People indexer
// $indexer = $tnt->createIndex('ketabnak.people');
// $indexer->setPrimaryKey('personid');
// $indexer->query('SELECT personid, name FROM persons;');
// $indexer->run();
function e($string) {

	return htmlspecialchars($string, ENT_QUOTES, 'UTF-8');
	
}


function convert($string) {
    $persian = ['۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹'];
    $arabic = ['٩', '٨', '٧', '٦', '٥', '٤', '٣', '٢', '١','٠'];

    $num = range(0, 9);
    $convertedPersianNums = str_replace($persian, $num, $string);
    $englishNumbersOnly = str_replace($arabic, $num, $convertedPersianNums);

    return $englishNumbersOnly;
}