$(document).ready(function(){
    $('#submit').on('click', function(){
        $("#error").remove();
        var title = $('#title').val();
        var dataString = 'title=' + title;

        // Validation message in case "title" is empty
        if(title == '') {
            $('#errors').append('<div id="error"><div class="mb-5 inline-flex items-center bg-white leading-none text-gray-900 rounded-full p-2 shadow text-teal text-sm w-full"><span class="inline-flex bg-red-600 text-white rounded-full h-6 px-3 justify-center items-center">توجه</span><span class="inline-flex px-2">لطفاً نام کتاب را وارد کنید </span></div></div>');
        } else {
            $.ajax({
                type: "POST",
                url: "../ketabnak/add-book.php",
                data: dataString,
                cache: false,
                success:function(html){
                    var parsed = $.parseJSON(html);
                    $('#books').append('<div class="p-2 w-full sm:w-1/2 md:w-1/3 lg:w-1/4"><div class="flex flex-row sm:flex-col items-center sm:items-start bg-white shadow-lg hover:shadow p-2 transition-all"><h2 class="font-extrabold text-3xl text-indigo-600">'+parsed['dlid']+'</h2><h3 class="mr-3 sm:mr-0 font-bold text-base">'+parsed['dltitle']+'</h3></div></div>');
                    
                    // Remove the previous title that has been already added to the database
                    $('#title').val("");
                }
            })
        }

        return false;
        
    });
});