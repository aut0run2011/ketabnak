$(document).ready(function(){


    var result_no = 12;


    $("#more_results").click(function(){
        event.preventDefault();
        result_no += 12;
        $('#title').trigger('change');
      });

    $('#title').on('change keyup paste', function(){
        $("#error").remove();
        var title = $('#title').val();
        var dataString = 'title=' + title + '&result_no=' + result_no;

        // Validation message in case "title" is empty
            $.ajax({
                type: "POST",
                url: "../ketabnak/search-book.php",
                data: dataString,
                cache: false,
                success:function(html){
                    var parsed = $.parseJSON(html);
                    // console.log(parsed[0]['title']);
                    $('#books').empty();
                    $('#errors').empty();
                    // console.log(parsed.length);
                    if(parsed.length == 0) {
                        $('#errors').append('<div id="error"><div class="mb-5 inline-flex items-center bg-white leading-none text-gray-900 rounded-full p-2 shadow text-teal text-sm w-full"><span class="inline-flex bg-red-600 text-white rounded-full h-6 px-3 justify-center items-center">توجه</span><span class="inline-flex px-2">متاسفانه جستجوی شما در کتاب‌ها نتیجه‌ای بر نداشت </span></div></div>');

                    }
                    $.each(parsed, function(i, item) {
                        $('#books').append('<div class="p-2 w-full sm:w-1/2 md:w-1/3 lg:w-1/4"><div class="flex flex-row sm:flex-col items-center sm:items-start bg-white shadow-lg hover:shadow p-2 transition-all"><h2 class="font-extrabold text-3xl text-indigo-600">'+parsed[i]['id']+'</h2><h3 class="mr-3 sm:mr-0 font-bold text-base">'+parsed[i]['title']+'</h3></div></div>');

                    });
                }
            });
        

            $.ajax({
                type: "POST",
                url: "../ketabnak/search-authors.php",
                data: dataString,
                cache: false,
                success:function(html){
                    var parsed = $.parseJSON(html);
                    // console.log(parsed[0]['title']);
                    $('#authors').empty();
                    $('#authors_errors').empty();
                    console.log(parsed);
                    if(parsed.length == 0) {
                        $('#authors_errors').append('<div id="error"><div class="mb-5 inline-flex items-center bg-white leading-none text-gray-900 rounded-full p-2 shadow text-teal text-sm w-full"><span class="inline-flex bg-red-600 text-white rounded-full h-6 px-3 justify-center items-center">توجه</span><span class="inline-flex px-2">متاسفانه جستجوی شما در نویسندگان نتیجه‌ای در بر نداشت </span></div></div>');

                    }
                    $.each(parsed, function(i, item) {
                        $('#authors').append('<div class="p-2 w-full sm:w-1/2 md:w-1/3 lg:w-1/4"><div class="flex flex-row sm:flex-col items-center sm:items-start bg-white shadow-lg hover:shadow p-2 transition-all"><h2 class="font-extrabold text-3xl text-indigo-600">'+parsed[i]['id']+'</h2><h3 class="mr-3 sm:mr-0 font-bold text-base">'+parsed[i]['title']+'</h3></div></div>');

                    });
                }
            });
        return false;
        
    });
});