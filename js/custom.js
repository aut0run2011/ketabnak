var h = document.documentElement,
b = document.body,
st = 'scrollTop',
sh = 'scrollHeight',
progress = document.querySelector('#progress'),
scroll;
var scrollpos = window.scrollY;
var header = document.getElementById("header");
var navcontent = document.getElementById("nav-content");
document.addEventListener('scroll', function() {
/*Refresh scroll % width*/
scroll = (h[st]||b[st]) / ((h[sh]||b[sh]) - h.clientHeight) * 100;
progress.style.setProperty('--scroll', scroll + '%');
/*Apply classes for slide in bar*/
scrollpos = window.scrollY;
if(scrollpos > 10){
header.classList.add("bg-blue-main");
header.classList.add("text-white");
header.classList.add("shadow-lg");
}
else {
header.classList.remove("bg-blue-main");
header.classList.remove("text-white");
header.classList.remove("shadow-lg");

}
});

