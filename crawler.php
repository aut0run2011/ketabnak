<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);

require 'vendor/autoload.php';


use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use Psr\Http\Message\ResponseInterface;
use GuzzleHttp\Exception\RequestException;


$starttime = microtime(true);
$client = new Client([
    // Base URI is used with relative requests
    'base_uri' => 'https://www.banasher.com/books',
    // You can set any number of default request options.
    'timeout'  => 2.0,
]);


$promise = $client->requestAsync('GET', 'http://httpbin.org/get');
$promise->then(
    function (ResponseInterface $res) {
        echo $res->getStatusCode() . "\n";
    },
    function (RequestException $e) {
        echo $e->getMessage() . "\n";
        echo $e->getRequest()->getMethod();
    }
);
$endtime = microtime(true); // Bottom of page
// printf("Page loaded in %f seconds", $endtime - $starttime);
// $client->getResponse();

